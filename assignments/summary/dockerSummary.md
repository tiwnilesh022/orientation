# Docker

- Docker is a set of platform as a service platform that uses OS-level virtualization to deliver software in packages called containers.
- Docker deploys softwares in containers which contains binaries, libraries and the application itself.

## Docker v/s Virtual Machine

![DocvsVM](https://user-images.githubusercontent.com/48025025/98109485-aa53b280-1ec3-11eb-9b0b-9836fc61a224.png)

- In Docker, the containers running share the host OS kernel. A Virtual Machine, on the other hand, is not based on container technology. They are made up of user space plus kernel space of an operating system.
- Both can be used to package up and distribute software. However, dockers are typically much smaller and faster than Virtual Machine, which makes them a much better fit for fast development cycles and microservices.

## Docker Images

- A Docker image is a file, comprised of multiple layers, that is used to execute code in a Docker container.
- An image is essentially built from the instructions for a complete and executable version of an application, which relies on the host OS kernel.

## Container

- A Docker container is a runtime instance of an image.
- From one image you can create multiple containers (all running the sample application) on multiple Docker platforms.
- You can create multiple containers for multiple applications in a docker.

# Docker Components:

## Docker Engine

- Docker Engine is one of the core components of Docker. It is responsible for the overall functioning of the Docker platform.
- Docker Engine is a client-server based application and consists of 3 main components.

Server: It is responsible for creating and managing Docker Images, Containers, Networks and Volumes on the Docker platform.
REST API: The REST API specifies how the applications can interact with the Server, and instruct it to get their job done.
Client: The Client is nothing but a command-line interface, that allows users to interact with Docker using the commands.

![doc](https://user-images.githubusercontent.com/48025025/98109984-70cf7700-1ec4-11eb-90b5-c03ca133478e.png)

## Docker Hub

Docker hub is the official online repository where you could find all the Docker Images that are available for us to use.

$ sudo apt-get remove docker docker-engine docker.io containerd runc

## Docker Installation

* Steps:

        $ sudo apt-get update
        $ sudo apt-get install \
            apt-transport-https \
            ca-certificates \
            curl \
            gnupg-agent \
            software-properties-common
        $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
        $ sudo apt-key fingerprint 0EBFCD88
        $ sudo add-apt-repository \
            "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
            $(lsb_release -cs) \
            stable nightly test"
        $ sudo apt-get update
        $ sudo apt-get install docker-ce docker-ce-cli containerd.io

        // Check if docker is successfully installed in your system
        $ sudo docker run hello-world

## Docker Basic commands

* `$ docker run hello world`- Docker hello world  
* `$ docker images` - Check number of docker images on your system  
* `$ docker search <image>` – Search an image in the Docker Hub   
* `$ docker run` – Runs a command in a new container.  
* `$ docker start` – Starts one or more stopped containers  
* `$ docker stop` – Stops one or more running containers  
* `$ docker build` – Builds an image form a Docker file  
* `$ docker pull` – Pulls an image or a repository from a registry  
* `$ docker push` – Pushes an image or a repository to a registry  
* `$ docker export` – Exports a container’s filesystem as a tar archive  
* `$ docker exec` – Runs a command in a run-time container  
* `$ docker search` – Searches the Docker Hub for images  
* `$ docker attach` – Attaches to a running container  
* `$ docker commit` – Creates a new image from a container’s changes  

